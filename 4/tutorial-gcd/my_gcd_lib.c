#include <my_gcd_lib.h>

int gcd(int x, int y){
  // compute the result following the pseudocode
  if (y == 0){
    return x;
  }
  else {
    return gcd(y,x%y);
  }
}

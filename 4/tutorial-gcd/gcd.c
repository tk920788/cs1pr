#include <stdio.h>
#include <stdlib.h>
#include <my_gcd_lib.h>

int main(int argc, char *argv[]){

  int a, b;

  a = atoi(argv[1]);
  b = atoi(argv[2]);

  printf("\nValues: %d and %d", a, b);
  printf("\ngcd(%d, %d) = %d\n\n", a, b, gcd(a, b));

  return 0;
}

// guards, you do not have to understand them, yet!
#ifndef MY_LIBRARY_H
#define MY_LIBRARY_H
// The declarations (prototypes) of functions go here
// Best to add some Doxygen documentation also
int gcd(int x, int y);

#endif

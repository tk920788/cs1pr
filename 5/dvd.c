#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

//#include <dvd.h>


// move this to DVD.h AND change the structure to match what a DVD should contain
struct data_t{
  char a[3];
  int b;
};

typedef struct data_t data_t;

int main(int argc, char ** argv){
  int ret;

  // open our database for reading and writing without removing any existing data! Mode: r+
  // note that this is a binary file, check the lecture about functions!
  FILE * fd = fopen("data.dat", "r+");
  if (fd == NULL){
    fd = fopen("data.dat", "w");
  }
  if (fd == NULL){
    // documented in the lecture about functions for proper error handling!
    printf("Error: %s\n", strerror(errno));
  }
  // check the slide about file IO in the lecture (5-functions), how to check and act upon an error returned by fopen

  // write the data, which is here a single record containing the information in the struct:
  data_t data = {.a = "ab", .b = 4711};

  // move to a position in the file, here position 8
  ret = fseek(fd, 8, SEEK_SET);

  // every read or write call will increment the "file pointer"
  ret = fwrite(& data, sizeof(data_t), 1, fd);
  if( ret == 0){
    printf("Error: %s\n", strerror(errno));
  }

  // read the data
  data_t dataRead;
  // move to the same position in the file that we wrote before
  ret = fseek(fd, 8, SEEK_SET);
  ret = fread(& dataRead, sizeof(data_t), 1, fd);
  if( ret == 0){
    printf("Error: %s\n", strerror(errno));
  }
  printf("Read: %s %d\n", dataRead.a, dataRead.b);

  fclose(fd);

  return 0;
}

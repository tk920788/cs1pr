#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// define the operations as enum here
enum operations{
  OP_MAX,
  OP_MEAN,
  OP_SD
};

/**
 * Your doxygen documentation of the function...
 * @Return ....
 */
int main(int argc, char ** argv){
  // TODO: parse command line arguments

  enum operations op;
  // 1. find the operator
  if (strcmp(argv[1], "max") == 0) {
    op = OP_MAX;
  }
  if (strcmp(argv[1], "mean") == 0) {
    op = OP_MEAN;
  }
  if (strcmp(argv[1], "sd") == 0) {
    op = OP_SD;
  }

  // 2. define an array with the necessary size

  int vector[argc-2];

  // 3. load numbers using my_atoi()

  // printf("\nargc %d", argc);

  for (int i=2; i<argc; i++){
    vector[i-2] = atoi(argv[i]);
  }

  // for (int i=0; i<argc-2; i++){
  //   printf("\n%d", vector[i]);
  // }

  // float (*fp)(int size, float * values); // declared function pointer variable
  //
  // // use a switch to set the function pointer
  // switch(op_var){
  //   case(OP_MAX):
  //     fp = ... // set the function pointer
  // }
  //
  // // invoke the function pointer
  // float result = fp(size, array)
  // printf("%.1f\n", result);

  return 0;
}

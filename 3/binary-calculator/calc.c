#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char ** argv){
  // read the arguments provided by the user, you do not have to understand how the next three lines work.
  // we use the uint8_t type which provides 8 bits
  int8_t dec1 = atoi(argv[1]);
  char op = argv[2][0]; // +, *, -, ...
  int8_t dec2 = atoi(argv[3]);

  // the following code constructs are just here to support you

  // print the arguments, for you to test, remove it before you submit!
  printf("Called with dec1: %d op: %c dec2: %d\n", (int) dec1, op, (int) dec2);

  // you need to check bitwise operators & and |
  // check if a bit is set, you need this when you perform the addition in binary!
  if(dec1 & 1){
    // bit with value 1 is set in dec1
    printf("lowest bit is 1\n");
  }
  printf("the value of 4|%d is %d\n", (int) dec1, dec1|4 );

  // check the bitshifting operators <<, note that you can shift by i!
  printf("the value of %d<<2 is %d\n", (int) dec1, dec1<<2);

  // make a decision based on the type of the operation
  if (op == '+'){
    // our iteration construct: think about how many times you must repeat something for the addition
    for (int i = 0; i < 4; i++){
      // do something, e.g., add the different bits
      printf("%d\n", i); // here we print the value of i
    }
  }
  printf("\n");

  int carry = 0;
  int x = dec1;
  int y = dec2;

  for (int i = 0; i < 4; i++){

    if(dec1 & 1){
      if (dec2 & 1){
        if (carry == 0){
          printf ("0");
          carry = 1;
        }
        if (carry == 1){
          printf ("1");
          carry = 1;
        }
      }
      else{
        if (carry == 0){
          printf ("1");
          carry = 0;
        }
        if (carry == 1){
          printf ("0");
          carry = 1;
        }
      }
    }
    else {
      if (dec2 & 1){
        if (carry == 0){
          printf ("1");
          carry = 0;
        }
        if (carry == 1){
          printf ("0");
          carry = 1;
        }
      }
      else{
        if (carry == 0){
          printf ("0");
          carry = 0;
        }
        if (carry == 1){
          printf ("1");
          carry = 0;
        }
      }
    }

    dec1 = dec1

  }


  return 0;
}
